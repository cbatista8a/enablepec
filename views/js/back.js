window.onload = function () {

  let invoice = document.getElementById('invoice');
  if (invoice!=null){
    EnableDisable(invoice);

    $(invoice).on('change',function () {
      EnableDisable(this);
    });
  }


}
//Hidde or Show Fields
function EnableDisable(invoice) {
  let pec = document.getElementById('pec');
  let sdi = document.getElementById('sdi');
  if (invoice.value == 0){
    pec.value= '';
    sdi.value= '';
    pec.parentElement.parentElement.style.display= 'none';
    sdi.parentElement.parentElement.style.display= 'none';
  }else {
    pec.parentElement.parentElement.style.display= '';
    sdi.parentElement.parentElement.style.display= '';
  }
}