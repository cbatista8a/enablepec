window.onload = function () {
  EnableDisable();
  let form = document.getElementsByClassName('address-form')[0];
  if (form){
    form.onchange = (function(e){
      setTimeout(EnableDisable,4000);
    });
  }

}



//Hidde or Show Fields
function EnableDisable() {
  let invoice = document.getElementsByName('invoice')[0];
  if (invoice){
    let pec = document.getElementsByName('pec')[0];
    let sdi = document.getElementsByName('sdi')[0];
    if (invoice.value == 0){
      pec.value= '';
      sdi.value= '';
      pec.parentElement.parentElement.style.display= 'none';
      sdi.parentElement.parentElement.style.display= 'none';
    }else {
      pec.parentElement.parentElement.style.display= '';
      sdi.parentElement.parentElement.style.display= '';
    }
    $(invoice).on('change',function () {
      EnableDisable();
    });
  }

}