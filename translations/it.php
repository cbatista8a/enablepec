<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{enablepec}prestashop>enablepec_972be12341b728a8ee8682f00177c979'] = 'Abilita i campi PEC e SDI';
$_MODULE['<{enablepec}prestashop>enablepec_b390813e3f9aa1154930fc492c27c051'] = 'Questo modulo consente di aggiungere campi PEC e SDI nell\'indirizzo del cliente.';
