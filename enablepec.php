<?php


if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * Class EnablePEC
 */
class Enablepec extends Module
{
    public function __construct()
    {
        $this->name         = 'enablepec';
        $this->version      = '1.0.1';
        $this->author       = 'OrangePix Srl.';

        parent::__construct();

        $this->bootstrap = true;

        $this->displayName  = $this->l('Enable PEC and SDI fields');
        $this->description  = $this->l('This module allows you to add PEC and SDI fields in customer address form.');

    }

    public function install()
    {
        include dirname(__FILE__).'/sql/install.php';

        if (!parent::install()
            || !$this->registerHook('header')
            || !$this->registerHook('BackOfficeHeader')
        ) {
            return false;
        }

        //Set custom Address Format for Italy Country
        $country = Country::getByIso('it');
        $addressformat = AddressFormat::getOrderedAddressFields($country);
        if (!in_array('pec',$addressformat)){
            $index = array_search('address1',$addressformat);
            array_splice($addressformat,$index,0,'pec');
            array_splice($addressformat,$index+1,0,'sdi');
            Db::getInstance()->update('address_format',['format'=> implode('\\n',$addressformat)],'id_country='.$country,0,false,false,true);
        }

        return true;
    }

    public function uninstall()
    {
        include dirname(__FILE__).'/sql/uninstall.php';

        $this->unregisterHook('header');
        $this->unregisterHook('BackOfficeHeader');

        //Reset Original Address Format for Italy Country
        $country = Country::getByIso('it');
        $addressformat = AddressFormat::getOrderedAddressFields($country);
        if (in_array('pec',$addressformat)){
            $index = array_search('pec',$addressformat);
            unset($addressformat[$index]);
            $index = array_search('sdi',$addressformat);
            unset($addressformat[$index]);
            Db::getInstance()->update('address_format',['format'=> implode('\\n',$addressformat)],'id_country='.$country,0,false,false,true);
        }

        //Delete override Files
        $this->DeleteOverrideFiles();

        parent::uninstall();

        return true;
    }

    //Scan files in ThisModule/override/ Folder
    public function ScanOverrideFiles($directory){
        $files = array();
        $dir = array_diff(scandir($directory), array('..', '.'));
        foreach ($dir as $folder){
            if (is_dir($directory.$folder)){
                $files[] = $this->ScanOverrideFiles($directory.$folder.'/');
            }else{
                $files[] = $directory.$folder;
            }
        }
        return $files;
    }

    //Delete files used by this module in rootProyect/override/ Folder
    public function DeleteOverrideFiles(){
        //Register Override Files for Uninstall
        $directory = $this->local_path.'override/';
        $overrides = json_encode($this->ScanOverrideFiles($directory)) ;
        $overrides = str_replace('[','',$overrides);
        $overrides = str_replace(']','',$overrides);
        $overrides = explode(',',stripslashes($overrides));


        foreach ($overrides as $file){
            $file =  explode('override/',trim($file,"\"") )[1];
            if (file_exists(_PS_OVERRIDE_DIR_.$file) && !is_dir(_PS_OVERRIDE_DIR_.$file)){
                try {
                    unlink(_PS_OVERRIDE_DIR_.$file);
                }catch (Exception $e){
                    $this->displayError($e->getMessage());
                    return false;
                }
            }
        }
        return true;
    }

    public function hookHeader(){
        $this->context->controller->addJS($this->_path.'views/js/front.js');
    }

    public function hookBackOfficeHeader(){
        $this->context->controller->addJS($this->_path.'views/js/back.js');
    }







}
