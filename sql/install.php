<?php

$sql = array();
$sql[]= 'ALTER TABLE '._DB_PREFIX_.'address ADD COLUMN `pec` varchar(250) AFTER `dni`;';
$sql[]= 'ALTER TABLE '._DB_PREFIX_.'address ADD COLUMN `sdi` varchar(250) AFTER `pec`;';
$sql[]= 'ALTER TABLE '._DB_PREFIX_.'address ADD COLUMN `invoice` tinyint(1) unsigned DEFAULT 0 ;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        continue;
    }
}
